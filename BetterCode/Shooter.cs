﻿using Microsoft.Xna.Framework;

using Microsoft.Xna.Framework.Graphics;

using System.Collections.Generic;

namespace BetterCode {
    class Shooter {
        public int timeBetweenShots;
        public int timeSinceLastShot;
        public bool canShoot;
        public List<Bullet> bullets = new List<Bullet>();

        GameTime gt;

        public Shooter(int _timeBetweenShots, float bulletSpeed, int bulletsTotal) {
            timeBetweenShots = _timeBetweenShots;
            timeSinceLastShot = 0;
            canShoot = true;
            for(int i = 0; i < bulletsTotal; i++) {
                Bullet newBullet = new Bullet(60, 16, 16, 2, bulletSpeed, 0, true);
                sendBulletToStagingPosition(newBullet);
                bullets.Add(newBullet);
            }
            
        }

        Bullet getAvailableBullet() {
            //dentre as Bullets fora da tela, selecione uma 
            int totalBullets = bullets.Count - 1;
            for(int i = 0; i < totalBullets; i++) {
                if (!bullets[i].isMoving) {
                    bullets[i].isMoving = true;
                    return bullets[i];

                }
            }
            return null;
        }

        public void shoot(Vector2 movementDirection, Vector2 origin) {
            //atira uma Bullet numa direção especificada
            if (canShoot) {
                Bullet bullet = getAvailableBullet();
                if (bullet != null) {
                    bullet.isMoving = true;
                    bullet.movementDirection = movementDirection;
                    //arrumar origin
                    Vector2 centerBullet = bullet.animation.frameDimensions / 2;
                    bullet.motion.position = origin - centerBullet;
                    canShoot = false;
                }
            }
        }

        public void shootRing(Vector2 origin) {
            //atira um anel de Bullets, uma em cada uma das oito direções
            Utilities util = new Utilities();
            if (canShoot) {
                Bullet bullet;
                for(int i = 0; i < 8; i++) {
                    bullet = getAvailableBullet();
                    if (bullet != null) {
                        bullet.isMoving = true;
                        bullet.movementDirection = util.facingDirectionToMovementDirection(i);
                        //arrumar origin
                        Vector2 centerBullet = bullet.animation.frameDimensions / 2;
                        bullet.motion.position = origin - centerBullet;
                    }
                }
                canShoot = false;
            }
        }

        public void DrawBullets(SpriteBatch spriteBatch) {
            //desenha as Bullets na tela
            int totalBullets = bullets.Count - 1;
            spriteBatch.Begin();
            for (int i = 0; i < totalBullets; i++) {
                if (bullets[i].isMoving) {
                    bullets[i].animation.DrawBatch(spriteBatch, bullets[i].motion.position);
                }

            }
            spriteBatch.End();
        }

        public static void UpdateBullets(object stateInfo) {
            Shooter localShooter = (Shooter)stateInfo;
            int totalBullets = localShooter.bullets.Count - 1;
            for (int i = 0; i < totalBullets; i++) {
                Bullet myBullet = localShooter.bullets[i];
                if (myBullet.isMoving) {
                    myBullet.Update(localShooter.gt);
                    if (myBullet.motion.position.X < -myBullet.collider.boundaries.Width ||
                       myBullet.motion.position.X > 1366 ||
                       myBullet.motion.position.Y < -myBullet.collider.boundaries.Height ||
                       myBullet.motion.position.Y > 786) {
                        localShooter.sendBulletToStagingPosition(myBullet);
                    }
                }
            }
            if (localShooter.timeSinceLastShot >= localShooter.timeBetweenShots) {
                localShooter.canShoot = true;
                localShooter.timeSinceLastShot = 0;
            } else {
                localShooter.timeSinceLastShot += localShooter.gt.ElapsedGameTime.Milliseconds;
            }
        }

        public void SetGameTime(GameTime gameTime) {
            gt = gameTime;
        }

        public bool checkBulletCollision(Bullet bullet, CollisionBox collider) {
            int totalBullets = bullets.Count - 1;
            if (bullet.collider.boundaries.Intersects(collider.boundaries)) {
                return true;
            }
            return false;
        }

        public void sendBulletToStagingPosition(Bullet bullet) {
            bullet.motion.position.X = -100;
            bullet.motion.position.Y = -100;
            bullet.isMoving = false;
            bullet.collider.Update(bullet.motion.position);
        }
    }
}