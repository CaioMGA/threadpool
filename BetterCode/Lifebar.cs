﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System;

namespace BetterCode {
    class Lifebar {
        public float life;
        public float maxLife;
        public float height;
        public float width;
        public Vector2 position;
        Vector2 frameLeftPosition;
        Vector2 frameRightPosition;
        public Texture2D fill;
        public Texture2D frameLeft;
        public Texture2D frameRight;
        public Texture2D frameMiddle;
        public Vector2 scale;
        public Vector2 frameScale;
        public Color fillColor;
        public Color frameColor;

        public Lifebar(float _life, float _maxLife, Vector2 _position, float _height, float _width, Color _fillColor, Color _frameColor ) {
            maxLife = _maxLife;
            life = _life;
            position = _position;
            height = _height;
            width = _width;
            scale = new Vector2(1, 1);
            frameScale = new Vector2(width, 1);
            fillColor = _fillColor;
            frameColor = _frameColor;
            frameLeftPosition.X = (int)position.X - 30;
            frameLeftPosition.Y = position.Y;
            frameRightPosition.X = (int)position.X + (int)width - 2;
            frameRightPosition.Y = position.Y;
        }

        public void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Begin();
            // desenha o meio da lifebar
            spriteBatch.Draw(fill, position, scale: scale, color: fillColor);

            //desenha moldura da lifebar
            if (frameLeft != null) {
                spriteBatch.Draw(frameLeft, frameLeftPosition, color: frameColor);
                spriteBatch.Draw(frameRight, frameRightPosition, color: frameColor);
                spriteBatch.Draw(frameMiddle, position, scale: frameScale, color: frameColor);
            }
            
            spriteBatch.End();
        }

        public void Update(int _life) {
            updateFillArea();
            life = _life;
        }

        public void Update() {
            updateFillArea();
        }

        public void updateFillArea() {
            float lifeRatio = life / maxLife;
            scale.X = (width * lifeRatio);
            scale.Y = (height / 32);

        }
    }
}
