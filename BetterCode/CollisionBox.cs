﻿using System;
using Microsoft.Xna.Framework;

namespace BetterCode {
    class CollisionBox {
        //Classe de colisão
        public Rectangle boundaries;
        public int collisionGroup;
        public bool isTrigger;

        public CollisionBox(Rectangle _boundaries, int _collisionGroup, bool _isTrigger) {
            boundaries = _boundaries;
            collisionGroup = _collisionGroup;
            isTrigger = _isTrigger;
        }


        public void Update(Vector2 pos) {
            boundaries.X = (int) pos.X;
            boundaries.Y = (int) pos.Y;
        }
    }

    
}
