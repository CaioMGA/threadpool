﻿using Microsoft.Xna.Framework;

namespace BetterCode {
    class Motion {
        public int facingDirection;
        /* 0 - UP
         * 1 - UPRIGHT
         * 2 - RIGHT
         * 3 - DOWNRIGHT
         * 4 - DOWN
         * 5 - DOWNLEFT
         * 6 - LEFT
         * 7 - UPLEFT
         */
        public float speed;
        public Vector2 position;
        public float hAxis; //movimento horizontal
        public float vAxis; //movimento vertical
        public bool aiming = false; 

        public Motion(float _speed, Vector2 _position) {
            speed = _speed;
            position = _position;

            hAxis = 0;
            vAxis = 0;
            facingDirection = 0;
        }

        public void Update(float _hAxis, float _vAxis, bool isAiming) {
            aiming = isAiming;
            solveFacingDirection();
            hAxis = _hAxis;
            vAxis = _vAxis;

            if (hAxis != 0 && vAxis != 0) {
                hAxis *= 0.76f;
                vAxis *= 0.76f;
            }
            if (!aiming) {
                position.X += speed * hAxis;
                position.Y += speed * vAxis;
            }
        }
        
        public void Update(Vector2 direction) {
            solveFacingDirection();
            hAxis = direction.X;
            vAxis = direction.Y;

            if (hAxis != 0 && vAxis != 0) {
                hAxis *= 0.76f;
                vAxis *= 0.76f;
            }
            position.X += speed * hAxis;
            position.Y += speed * vAxis;
        }

        void solveFacingDirection() {
            /* 0 - UP
             * 1 - UPRIGHT
             * 2 - RIGHT
             * 3 - DOWNRIGHT
             * 4 - DOWN
             * 5 - DOWNLEFT
             * 6 - LEFT
             * 7 - UPLEFT
             */

            if (vAxis > 0) { //down
                if (hAxis > 0) { // right
                    facingDirection = 3;
                } else if (hAxis < 0) { // left
                    facingDirection = 5;
                } else { // just down
                    facingDirection = 4;
                }
            } else if (vAxis < 0) { //up
                if (hAxis > 0) { // right
                    facingDirection = 1;
                } else if (hAxis < 0) { // left
                    facingDirection = 7;
                } else { // just up
                    facingDirection = 0;
                }
            } else { // not up and not down
                if (hAxis > 0) { // right
                    facingDirection = 2;
                } else if (hAxis < 0) { // left
                    facingDirection = 6;
                }
            }
        }
    }
}
