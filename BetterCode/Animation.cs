﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

//handles animations

namespace BetterCode {
    class Animation {
        public int currentFrame;
        public int framesPerAnimation;
        public int facingDirection;
        int facingDirectionInPreviousFrame;
        /* 0 - UP
         * 1 - UPRIGHT
         * 2 - RIGHT
         * 3 - DOWNRIGHT
         * 4 - DOWN
         * 5 - DOWNLEFT
         * 6 - LEFT
         * 7 - UPLEFT
         */
        public int timeBetweenFrames;
        public int timeSincePreviousFrame;

        public Texture2D spriteSheet;
        public Rectangle crop;
        public Vector2 frameDimensions;

        bool idle;
        public bool useFacingDirection = true;

        public Animation(int _timeBetweenFrames, Vector2 _frameDimensions, int _framesPerAnimation) {
            //carregar imagem spritesheet no Game1
            timeBetweenFrames = _timeBetweenFrames; // milissegundos
            frameDimensions = _frameDimensions;
            framesPerAnimation = _framesPerAnimation;

            currentFrame = 0;
            crop = new Rectangle(0, 0, (int)frameDimensions.X, (int)frameDimensions.Y);
            facingDirection = 0;
            facingDirectionInPreviousFrame = 0;
            idle = true;
        }

        public void Update(GameTime _gameTime, int _facingDirection, bool _idle) {
            solveFrame(_gameTime);
            if (useFacingDirection) {
                solveCrop(_facingDirection);
            }

            idle = _idle;
        }

        public void Update(GameTime _gameTime) {
            solveFrame(_gameTime);
        }

        void solveFrame(GameTime gameTime) {
            if (useFacingDirection) {
                if (facingDirection != facingDirectionInPreviousFrame) {
                    currentFrame = 2;
                    timeSincePreviousFrame = 0;
                }
                facingDirectionInPreviousFrame = facingDirection;
            }
            

            if (idle) {
                currentFrame = 1;
            } else {
                if (timeSincePreviousFrame >= timeBetweenFrames) {
                    timeSincePreviousFrame = 0;
                    currentFrame++;
                    if (currentFrame > 3) {
                        currentFrame = 0;
                    }
                } else {
                    timeSincePreviousFrame += gameTime.ElapsedGameTime.Milliseconds;
                }
            }

            
        }

        void solveCrop(int facingDirection) {
            //dependendo da animação utilize uma linha diferente na Spritesheet
            int rowInSpritesheet = 0;

            if (facingDirection == 0) {
                rowInSpritesheet = 7;
            }
            if (facingDirection == 1) {
                rowInSpritesheet = 6;
            }
            if (facingDirection == 2) {
                rowInSpritesheet = 5;
            }
            if (facingDirection == 3) {
                rowInSpritesheet = 2;
            }
            if (facingDirection == 4) {
                rowInSpritesheet = 1;
            }
            if (facingDirection == 5) {
                rowInSpritesheet = 0;
            }
            if (facingDirection == 6) {
                rowInSpritesheet = 3;
            }
            if (facingDirection == 7) {
                rowInSpritesheet = 4;
            }

            crop.X = (int)frameDimensions.X * currentFrame;
            crop.Y = rowInSpritesheet * (int)frameDimensions.Y;
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 position) {
            spriteBatch.Begin();
            spriteBatch.Draw(
                spriteSheet,
                position,
                sourceRectangle: crop
                );
            spriteBatch.End();
        }

        public void DrawBatch(SpriteBatch spriteBatch, Vector2 position) {
            spriteBatch.Draw(
                spriteSheet,
                position,
                sourceRectangle: crop
                );
        }
    }
}
