using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

/* TO DO
 * Criar threads e rodar métodos dentro das threads
 * paralelizar os updates
 */

namespace BetterCode {

    public class Game1 : Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Hero hero;
        Boss boss;
        Tourinion tourinion;

        public Game1() {
            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = true;
            graphics.PreferredBackBufferWidth = 1366;
            graphics.PreferredBackBufferHeight = 768;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize() {

            hero = new Hero(100, 32, 50, 4, 4, 100, 600, 0, true, 100, 100, 1);
            boss = new Boss(100, 64, 64, 10, 4, 300, 200, 0, true, 400, 5);
            tourinion = new Tourinion(100, 64, 64, 10, 4, 300, 400, 0, true, 400, 5);

            boss.lifebar = new Lifebar(boss.life.life, boss.life.life, new Vector2(494, 10), 32, 840, Color.Blue, Color.DeepSkyBlue);
            hero.lifebar = new Lifebar(hero.life.life, hero.life.life, new Vector2(32, 726), 32, 640, Color.Red, Color.Gold);
            base.Initialize();
        }

        protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            hero.animation.spriteSheet = Content.Load<Texture2D>("ness");
            boss.animation.spriteSheet = Content.Load<Texture2D>("spritesheet_testes");
            tourinion.animation.spriteSheet = Content.Load<Texture2D>("spritesheet_testes");

            hero.lifebar.fill = Content.Load<Texture2D>("lifebar_fill");
            hero.lifebar.frameLeft = Content.Load<Texture2D>("frame_left");
            hero.lifebar.frameRight = Content.Load<Texture2D>("frame_right_alt");
            hero.lifebar.frameMiddle = Content.Load<Texture2D>("frame_mid");

            boss.lifebar.fill = Content.Load<Texture2D>("lifebar_fill");
            boss.lifebar.frameLeft = Content.Load<Texture2D>("frame_left_alt");
            boss.lifebar.frameRight = Content.Load<Texture2D>("frame_right");
            boss.lifebar.frameMiddle = Content.Load<Texture2D>("frame_mid");

            hero.lifebar.Update();
            boss.lifebar.Update();

            loadBulletSprites();
        }

        protected override void UnloadContent() {

        }

        protected override void Update(GameTime gameTime) {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            hero.Update(gameTime);
            boss.Update(gameTime);
            tourinion.Update(gameTime);
            //hero.lifebar.Update();
            //boss.lifebar.Update();

            testCollision();
            hero.shooter.UpdateBullets(gameTime);
            testHeroShots();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            hero.animation.Draw(spriteBatch, hero.motion.position);
            hero.lifebar.Draw(spriteBatch);

            boss.animation.Draw(spriteBatch, boss.motion.position);
            boss.lifebar.Draw(spriteBatch);

            tourinion.animation.Draw(spriteBatch, tourinion.motion.position);

            hero.shooter.DrawBullets(spriteBatch);

            base.Draw(gameTime);
        }

        void loadBulletSprites() {
            int totalBullets = hero.shooter.bullets.Count - 1;
            for(int i = 0; i < totalBullets ; i++) {
                hero.shooter.bullets[i].animation.spriteSheet = Content.Load<Texture2D>("bullet1");
            }
        }

        void testCollision() {
            if (hero.collider.boundaries.Intersects(boss.collider.boundaries)){
                hero.life.takeDamage(boss.life.damage);
                hero.lifebar.Update(hero.life.life);
                hero.life.setTemporaryInvencibility();
            }
        }

        public void testHeroShots() {
            int totalHeroBullets = hero.shooter.bullets.Count - 1;

            for(int i = 0; i < totalHeroBullets; i++) {
                if (hero.shooter.checkBulletCollision(hero.shooter.bullets[i], boss.collider)) {
                    hero.shooter.sendBulletToStagingPosition(hero.shooter.bullets[i]);
                    boss.life.takeDamage(hero.life.damage);
                    boss.lifebar.Update(boss.life.life);
                }
            }
        }
    }
}
