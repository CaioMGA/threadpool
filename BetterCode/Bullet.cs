﻿using Microsoft.Xna.Framework;

namespace BetterCode {
    class Bullet {
        public Animation animation;
        public Motion motion;
        public CollisionBox collider;
        
        public bool isMoving;
        public Vector2 movementDirection;

        public Bullet(int animationFrameTime, int frameWidth, int frameHeight, int framesPerAnimation, float speed, int collisionGroup, bool isTrigger) {
            animation = new Animation(animationFrameTime, new Vector2(frameWidth, frameHeight), framesPerAnimation);
            motion = new Motion(speed, new Vector2(0, 0));
            Rectangle collisionBoxBoundaries = new Rectangle(0, 0, frameWidth, frameHeight);
            collider = new CollisionBox(collisionBoxBoundaries, collisionGroup, isTrigger);
            isMoving = false;
            movementDirection = new Vector2(0, 0); //parado
        }

        public void Update(GameTime gameTime) {
            motion.Update(movementDirection);
            animation.Update(gameTime, motion.facingDirection, false);
            collider.Update(motion.position);
        }
    }
}
