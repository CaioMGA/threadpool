﻿using System;
using System.Linq;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BetterCode {
    class Menu {
        public SpriteFont bigFont;
        public SpriteFont mediumFont;
        public SpriteFont smallFont;

        SpriteBatch spriteBatch;

        public String gameState;
        bool keyPressed;
        
        public Menu(SpriteBatch _spritebatch, String _gameState = "Credits") {
            gameState = _gameState;
            spriteBatch = _spritebatch;
            keyPressed = false;
        }

        public void Update() {
            // processa input do jogador de acordo com o menu sendo mostrado
            if (!keyPressed) {
                if (gameState.Equals("Credits")) {
                    if (Keyboard.GetState().GetPressedKeys().Any()) {
                        gameState = "Main Menu";
                        keyPressed = true;
                    }
                } else if (gameState.Equals("Main Menu")) {
                    if (Keyboard.GetState().IsKeyDown(Keys.Space)) {
                        gameState = "Credits";
                        keyPressed = true;
                    } else if (Keyboard.GetState().IsKeyDown(Keys.Enter)) {
                        gameState = "PreGame";
                        keyPressed = true;
                    } else if (Keyboard.GetState().IsKeyDown(Keys.Escape)) {
                        gameState = "Quit";
                        keyPressed = true;
                    } else if (Keyboard.GetState().IsKeyDown(Keys.LeftControl) || Keyboard.GetState().IsKeyDown(Keys.RightControl)) {
                        gameState = "How To Play";
                        keyPressed = true;
                    }
                } else if (gameState.Equals("Game")) {
                    if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape)) {
                        gameState = "Pause";
                        keyPressed = true;
                    } else if (Keyboard.GetState().GetPressedKeys().Any()) {
                        keyPressed = true;
                    }
                } else if (gameState.Equals("How To Play")) {
                    if (Keyboard.GetState().IsKeyDown(Keys.Space)) {
                        gameState = "Main Menu";
                        keyPressed = true;
                    } else if (Keyboard.GetState().IsKeyDown(Keys.Enter)) {
                        gameState = "Game";
                        keyPressed = true;
                    } else if (Keyboard.GetState().IsKeyDown(Keys.Escape)) {
                        gameState = "Quit";
                        keyPressed = true;
                    }
                } else if (gameState.Equals("Pause")) {
                    if (Keyboard.GetState().IsKeyDown(Keys.Space)) {
                        gameState = "Main Menu";
                        keyPressed = true;
                    } else if (Keyboard.GetState().IsKeyDown(Keys.Enter)) {
                        gameState = "Game";
                        keyPressed = true;
                    } else if (Keyboard.GetState().IsKeyDown(Keys.Escape)) {
                        gameState = "Quit";
                        keyPressed = true;
                    }
                } else if (gameState.Equals("You Win")) {
                    if (Keyboard.GetState().GetPressedKeys().Any()) {
                        gameState = "Main Menu";
                        keyPressed = true;
                    }
                } else if (gameState.Equals("You Lose")) {
                    if (Keyboard.GetState().IsKeyDown(Keys.Space)) {
                        gameState = "Main Menu";
                        keyPressed = true;
                    } else if (Keyboard.GetState().IsKeyDown(Keys.Enter)) {
                        gameState = "PreGame";
                        keyPressed = true;
                    } else if (Keyboard.GetState().IsKeyDown(Keys.Escape)) {
                        gameState = "Quit";
                        keyPressed = true;
                    }
                } else if (gameState.Equals("Quit")) {

                }
                
            } else if (Keyboard.GetState().GetPressedKeys().Length == 0) {
                keyPressed = false;
            }
        }

        public void Draw() {
            //desenha um determinado menu de acordo com o gameState
            if (gameState.Equals("Credits")) {
                showCredits();
            } else if (gameState.Equals("Main Menu")) {
                showMainMenu();
            } else if (gameState.Equals("How To Play")) {
                showHowToPlay();
            } else if (gameState.Equals("Game")) {
                showGame();
            } else if (gameState.Equals("Pause")) {
                showPause();
            } else if (gameState.Equals("You Win")) {
                showYouWin();
            } else if (gameState.Equals("You Lose")) {
                showYouLose();
            } else if (gameState.Equals("Quit")) {
                showQuit();
            }
        }

        void showCredits() {
            //sombras

            Vector2 textPosition = new Vector2(25, 25);
            spriteBatch.Begin();
            spriteBatch.DrawString(bigFont, "THREADPOOL", textPosition, Color.DarkSlateGray);
            textPosition.Y += 97;
            textPosition.X = 22;
            spriteBatch.DrawString(mediumFont, "Game usando Processamento Paralelo", textPosition, Color.DarkSlateGray);
            textPosition.Y += 60;
            spriteBatch.DrawString(mediumFont, "Alexandre Ligório", textPosition, Color.DarkSlateGray);
            textPosition.Y += 60;
            spriteBatch.DrawString(mediumFont, "Caio Marchi", textPosition, Color.DarkSlateGray);
            textPosition.Y += 90;
            spriteBatch.DrawString(mediumFont, "Professor: Jean M. Laine", textPosition, Color.DarkSlateGray);
            textPosition.Y += 60;
            spriteBatch.DrawString(mediumFont, "Disciplina: Processamento Paralelo", textPosition, Color.DarkSlateGray);
            textPosition.Y += 90;
            spriteBatch.DrawString(mediumFont, "Jogos Digitais", textPosition, Color.DarkSlateGray);
            textPosition.Y += 60;
            spriteBatch.DrawString(mediumFont, "FATEC Carapicuíba - Junho 2016", textPosition, Color.DarkSlateGray);
            textPosition.Y += 89;
            spriteBatch.DrawString(mediumFont, "Músicas por TannerHelland.com", textPosition, Color.DarkSlateGray);
            textPosition.Y += 60;
            spriteBatch.DrawString(smallFont, " - Pressione qualquer tecla para prosseguir - ", textPosition, Color.DarkSlateGray);


            //texto branco
            textPosition = new Vector2(20, 20);
            spriteBatch.DrawString(bigFont, "THREADPOOL", textPosition, Color.White);
            textPosition.Y += 100;
            spriteBatch.DrawString(mediumFont, "Game usando Processamento Paralelo", textPosition, Color.White);
            textPosition.Y += 60;
            spriteBatch.DrawString(mediumFont, "Alexandre Ligório", textPosition, Color.White);
            textPosition.Y += 60;
            spriteBatch.DrawString(mediumFont, "Caio Marchi", textPosition, Color.White);
            textPosition.Y += 90;
            spriteBatch.DrawString(mediumFont, "Professor: Jean M. Laine", textPosition, Color.White);
            textPosition.Y += 60;
            spriteBatch.DrawString(mediumFont, "Disciplina: Processamento Paralelo", textPosition, Color.White);
            textPosition.Y += 90;
            spriteBatch.DrawString(mediumFont, "Jogos Digitais", textPosition, Color.White);
            textPosition.Y += 60;
            spriteBatch.DrawString(mediumFont, "FATEC Carapicuíba - Junho 2016", textPosition, Color.White);
            textPosition.Y += 90;
            spriteBatch.DrawString(mediumFont, "Músicas por TannerHelland.com", textPosition, Color.White);
            textPosition.Y += 60;
            spriteBatch.DrawString(smallFont, " - Pressione qualquer tecla para prosseguir - ", textPosition, Color.White);
            spriteBatch.End();
        }

        void showMainMenu() {
            Vector2 textPosition = new Vector2(395, 205);
            spriteBatch.Begin();
            //sombras
            spriteBatch.DrawString(bigFont, "THREADPOOL", textPosition, Color.DarkSlateGray);
            textPosition.Y += 87;
            textPosition.X = 393;
            spriteBatch.DrawString(mediumFont, "Começar o jogo [ENTER]", textPosition, Color.DarkSlateGray);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Créditos [ESPAÇO]", textPosition, Color.DarkSlateGray);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Como Jogar[CTRL]", textPosition, Color.DarkSlateGray);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Sair [ESC]", textPosition, Color.DarkSlateGray);

            //texto branco
            textPosition = new Vector2(390, 200);
            spriteBatch.DrawString(bigFont, "THREADPOOL", textPosition, Color.White);
            textPosition.Y += 90;
            spriteBatch.DrawString(mediumFont, "Começar o jogo [ENTER]", textPosition, Color.White);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Créditos [ESPAÇO]", textPosition, Color.White);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Como Jogar[CTRL]", textPosition, Color.White);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Sair [ESC]", textPosition, Color.White);
            spriteBatch.End();
        }

        void showGame() {
            Vector2 textPosition;
            spriteBatch.Begin();
            //sombras
            textPosition = new Vector2(1279, 24);
            spriteBatch.DrawString(smallFont, "BOSS", textPosition, Color.Black);
            textPosition = new Vector2(39, 734);
            spriteBatch.DrawString(smallFont, "Player", textPosition, Color.Black);

            textPosition = new Vector2(1281, 26);
            spriteBatch.DrawString(smallFont, "BOSS", textPosition, Color.Black);
            textPosition = new Vector2(41, 736);
            spriteBatch.DrawString(smallFont, "Player", textPosition, Color.Black);

            textPosition = new Vector2(1282, 27);
            spriteBatch.DrawString(smallFont, "BOSS", textPosition, Color.Black);
            textPosition = new Vector2(42, 737);
            spriteBatch.DrawString(smallFont, "Player", textPosition, Color.Black);

            //branco
            textPosition = new Vector2(1280, 25);
            spriteBatch.DrawString(smallFont, "BOSS", textPosition, Color.White);
            textPosition = new Vector2(40, 735);
            spriteBatch.DrawString(smallFont, "Player", textPosition, Color.White);
            spriteBatch.End();
        }

        void showHowToPlay() {
            Vector2 textPosition = new Vector2(105, 85);
            spriteBatch.Begin();
            //sombras
            spriteBatch.DrawString(bigFont, "Como Jogar", textPosition, Color.DarkSlateGray);
            textPosition.Y += 107;
            textPosition.X = 103;
            spriteBatch.DrawString(mediumFont, "[A][S][D][W] atiram [setas direcionais] movimentam", textPosition, Color.DarkSlateGray);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "O inimigo que pisca é o Boss, acerte-o.", textPosition, Color.DarkSlateGray);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "O inimigo branco absorve tiros, tanto seus quanto do Boss.", textPosition, Color.DarkSlateGray);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Jogar [ENTER]", textPosition, Color.DarkSlateGray);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Main Menu [ESPAÇO]", textPosition, Color.DarkSlateGray);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Sair [ESC]", textPosition, Color.DarkSlateGray);


            //texto branco
            textPosition = new Vector2(100, 80);
            spriteBatch.DrawString(bigFont, "Como Jogar", textPosition, Color.White);
            textPosition.Y += 110;
            spriteBatch.DrawString(mediumFont, "[A][S][D][W] atiram [setas direcionais] movimentam", textPosition, Color.White);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "O inimigo que pisca é o Boss, acerte-o.", textPosition, Color.White);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "O inimigo branco absorve tiros, tanto seus quanto do Boss.", textPosition, Color.White);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Jogar [ENTER]", textPosition, Color.White);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Main Menu [ESPAÇO]", textPosition, Color.White);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Sair [ESC]", textPosition, Color.White);
            spriteBatch.End();
        }

        void showPause() {
            Vector2 textPosition = new Vector2(495, 205);
            spriteBatch.Begin();
            
            //sombras
            spriteBatch.DrawString(bigFont, "PAUSE", textPosition, Color.DarkSlateGray);
            textPosition.Y += 87;
            textPosition.X = 493;
            spriteBatch.DrawString(mediumFont, "Retomar Jogo [ENTER]", textPosition, Color.DarkSlateGray);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Main Menu [ESPAÇO]", textPosition, Color.DarkSlateGray);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Sair [ESC]", textPosition, Color.DarkSlateGray);


            //texto branco
            textPosition = new Vector2(490, 200);
            spriteBatch.DrawString(bigFont, "PAUSE", textPosition, Color.White);
            textPosition.Y += 90;
            spriteBatch.DrawString(mediumFont, "Retomar Jogo [ENTER]", textPosition, Color.White);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Main Menu [ESPAÇO]", textPosition, Color.White);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Sair [ESC]", textPosition, Color.White);
            spriteBatch.End();
        }

        void showYouWin() {
            Vector2 textPosition = new Vector2(395, 205);
            spriteBatch.Begin();
            //sombras
            spriteBatch.DrawString(bigFont, "Você Venceu!", textPosition, Color.DarkSlateGray);
            textPosition.Y += 87;
            textPosition.X = 177;
            spriteBatch.DrawString(mediumFont, " - Pressione Qualquer tecla para prosseguir - ", textPosition, Color.DarkSlateGray);

            //texto branco
            textPosition = new Vector2(390, 200);
            spriteBatch.DrawString(bigFont, "Você Venceu!", textPosition, Color.White);
            textPosition.Y += 90;
            textPosition.X = 180;
            spriteBatch.DrawString(mediumFont, " - Pressione Qualquer tecla para prosseguir - ", textPosition, Color.White);
            spriteBatch.End();
        }

        void showYouLose() {
            Vector2 textPosition = new Vector2(395, 205);
            spriteBatch.Begin();
            //sombras
            spriteBatch.DrawString(bigFont, "Você Perdeu!", textPosition, Color.DarkSlateGray);
            textPosition.Y += 87;
            textPosition.X = 393;
            spriteBatch.DrawString(mediumFont, "Tentar novamente [ENTER]", textPosition, Color.DarkSlateGray);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Main Menu [ESPAÇO]", textPosition, Color.DarkSlateGray);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Sair [ESC]", textPosition, Color.DarkSlateGray);

            //texto branco
            textPosition = new Vector2(390, 200);
            spriteBatch.DrawString(bigFont, "Você Perdeu!", textPosition, Color.White);
            textPosition.Y += 90;
            spriteBatch.DrawString(mediumFont, "Tentar novamente [ENTER]", textPosition, Color.White);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Main Menu [ESPAÇO]", textPosition, Color.White);
            textPosition.Y += 80;
            spriteBatch.DrawString(mediumFont, "Sair [ESC]", textPosition, Color.White);
            spriteBatch.End();
        }

        void showQuit() {

        }
    }

    
}
