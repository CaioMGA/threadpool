﻿using Microsoft.Xna.Framework.Input;

namespace BetterCode {
    class Input {
        public float hAxis; // movimento horizontal
        public float vAxis; // movimento vertical
        public float vShootingAxis; // disparo no eixo vertical
        public float hShootingAxis; // disparo no eixo horizontal
        public bool idle;
        public bool isShootButtonPress;
        public bool isAimingButtonPress;

        public Input() {
            hAxis = 0;
            vAxis = 0;

            vShootingAxis = 0;
            hShootingAxis = 0;

            idle = true;
        }

        public void Update() {
            hAxis = 0;
            vAxis = 0;
            if (Keyboard.GetState().IsKeyDown(Keys.Up)) {
                vAxis -= 1;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down)) {
                vAxis += 1;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Left)) {
                hAxis -= 1;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right)) {
                hAxis += 1;
            }
            //ide
            if (vAxis == hAxis && vAxis == 0) {
                idle = true;
            } else {
                idle = false;
            }
            //aiming
            if (Keyboard.GetState().IsKeyDown(Keys.LeftShift)) {
                isAimingButtonPress = true;
            }
            if (Keyboard.GetState().IsKeyUp(Keys.LeftShift)) {
                isAimingButtonPress = false;
            }

            vShootingAxis = 0;
            hShootingAxis = 0;
            isShootButtonPress = false;

            if (Keyboard.GetState().IsKeyDown(Keys.W)){
                vShootingAxis = -1;
                isShootButtonPress = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.S)){
                vShootingAxis = 1;
                isShootButtonPress = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.A)){
                hShootingAxis = -1;
                isShootButtonPress = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D)){
                hShootingAxis = 1;
                isShootButtonPress = true;
            }

            
        }
    }
}
