﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace BetterCode {
    class Boss {
        public Animation animation;
        public Motion motion;
        public CollisionBox collider;
        public Life life;
        public Lifebar lifebar;
        public Shooter shooter;

        public Vector2 centerSprite;
        public Vector2 center;

        public Vector2[] circuit; // posições que o Boss andará, assim que cegar a uma posição, segue para a próxima
        public int circuitPosition = 0;
        GameTime gt;

        //sons
        public SoundEffect shot;
        SoundEffectInstance shotInstance;

        public void createSoundEffectInstance() {
            shotInstance = shot.CreateInstance();
        }

        public Boss(int animationFrameTime, int frameWidth, int frameHeight, int framesPerAnimation, float speed, int posX, int posY, int collisionGroup, bool isTrigger, int _life, int _damage) {
            animation = new Animation(animationFrameTime, new Vector2(frameWidth, frameHeight), framesPerAnimation);
            motion = new Motion(speed, new Vector2(posX, posY));
            Rectangle collisionBoxBoundaries = new Rectangle(posX, posY, frameWidth, frameHeight);
            collider = new CollisionBox(collisionBoxBoundaries, collisionGroup, isTrigger);
            life = new Life(_life, _damage);
            shooter = new Shooter(1000, 6f, 30);
            shooter.canShoot = false;
            centerSprite = new Vector2(frameWidth / 2, frameHeight / 2);
            circuit = new Vector2[] {
                new Vector2(1200, 100),
                new Vector2(1200, 660),
                new Vector2(100, 660),
                new Vector2(100, 100)
            };
        }

        public static void Update(object stateInfo) {
            Boss localBoss = (Boss)stateInfo;
            Vector2 movementDirection = localBoss.setDirection();
            localBoss.motion.Update(movementDirection);
            localBoss.animation.Update(localBoss.gt, localBoss.motion.facingDirection, false);
            localBoss.collider.boundaries.X = (int)localBoss.motion.position.X;
            localBoss.collider.boundaries.Y = (int)localBoss.motion.position.Y;
            localBoss.life.Update(localBoss.gt);
            localBoss.UpdateCenter();
            if (localBoss.shooter.canShoot) {
                localBoss.shotInstance.Play();
            }
            localBoss.shooter.shootRing(localBoss.center);
        }

        public void SetGameTime(GameTime gameTime) {
            gt = gameTime;
        }

        void UpdateCenter() {
            center = centerSprite + motion.position;    
        }

        Vector2 setDirection() {
            Vector2 dir;
            if (circuitPosition == 0) {
                //anda para a direita
                if (motion.position.X < circuit[circuitPosition].X) {
                    dir = new Vector2(1, 0);
                    return dir;
                } else {
                    circuitPosition = 1;
                }
            } if (circuitPosition == 1) {
                //anda para a baixo
                if (motion.position.Y < circuit[circuitPosition].Y) {
                    dir = new Vector2(0, 1);
                    return dir;
                } else {
                    circuitPosition = 2;
                }
            }if (circuitPosition == 2) {
                //anda para a esquerda
                if (motion.position.X > circuit[circuitPosition].X) {
                    dir = new Vector2(-1, 0);
                    return dir;
                } else {
                    circuitPosition = 3;
                }
            }if (circuitPosition == 3) {
                //anda para cima
                if (motion.position.Y > circuit[circuitPosition].Y) {
                    dir = new Vector2(0, -1);
                    return dir;
                } else {
                    circuitPosition = 0;
                }
            }
            dir = new Vector2(0, 0);
            return dir;
        }
    }
}
