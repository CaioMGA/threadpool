﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

using System.Threading;

namespace BetterCode {

    public class Game1 : Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Hero hero;
        Boss boss;
        Tourinion tourinion;

        Texture2D floor;
        Vector2 floorPosition;
        Rectangle floorRect;

        String gameState;

        //variaveis de menu
        Menu menu;

        //Sons
        Song mainGameSong;
        Song mainMenuSong;
        int currentSong;
        SoundEffect heroHit;
        SoundEffect bossHit;
        SoundEffect tourinionHit;

        public Game1() {
            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = true;
            graphics.PreferredBackBufferWidth = 1366;
            graphics.PreferredBackBufferHeight = 768;
            Content.RootDirectory = "Content";
            //ThreadPool.SetMaxThreads(5, 5);
            gameState = "Credits";

        }
        

        protected override void Initialize() {
            //instancia hero, boss e miniboss
            hero = new Hero(100, 32, 50, 4, 6f, 100, 600, 0, true, 100, 100, 1);
            boss = new Boss(100, 64, 64, 10, 1f, 100, 100, 0, true, 400, 10);
            tourinion = new Tourinion(100, 64, 64, 10, 1f, 1200, 100, 0, true, 400, 5, hero);

            boss.lifebar = new Lifebar(boss.life.life, boss.life.life, new Vector2(494, 10), 32, 840, Color.Red, Color.Silver);
            hero.lifebar = new Lifebar(hero.life.life, hero.life.life, new Vector2(32, 726), 32, 640, Color.ForestGreen, Color.Goldenrod);


            floorPosition = new Vector2(0, 0);
            floorRect = new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);

            currentSong = 0;
            MediaPlayer.Volume = 1f;
            MediaPlayer.IsRepeating = true;

            base.Initialize();
        }

        protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //carrega as imagens do jogo
            hero.animation.spriteSheet = Content.Load<Texture2D>("ness");
            boss.animation.spriteSheet = Content.Load<Texture2D>("spritesheet_testes");
            tourinion.animation.spriteSheet = Content.Load<Texture2D>("spritesheet_testes_tourinion");

            hero.lifebar.fill = Content.Load<Texture2D>("lifebar_fill");
            hero.lifebar.frameLeft = Content.Load<Texture2D>("frame_left");
            hero.lifebar.frameRight = Content.Load<Texture2D>("frame_right_alt");
            hero.lifebar.frameMiddle = Content.Load<Texture2D>("frame_mid");

            boss.lifebar.fill = Content.Load<Texture2D>("lifebar_fill");
            boss.lifebar.frameLeft = Content.Load<Texture2D>("frame_left_alt");
            boss.lifebar.frameRight = Content.Load<Texture2D>("frame_right");
            boss.lifebar.frameMiddle = Content.Load<Texture2D>("frame_mid");

            hero.lifebar.Update();
            boss.lifebar.Update();

            loadBulletSprites();
            floor = Content.Load<Texture2D>("BrickFloorFinal");

            //menu 
            menu = new Menu(spriteBatch);
            menu.bigFont = Content.Load<SpriteFont>("BigText");
            menu.mediumFont = Content.Load<SpriteFont>("MediumText");
            menu.smallFont = Content.Load<SpriteFont>("SmallText");

            //sons
            mainGameSong = Content.Load<Song>("Sound/MainGameMusic");
            mainMenuSong = Content.Load<Song>("Sound/MainMenuMusic");
            MediaPlayer.Play(mainMenuSong);

            hero.shot = Content.Load<SoundEffect>("Sound/playerShot");
            boss.shot = Content.Load<SoundEffect>("Sound/bossRingShot");
            heroHit = Content.Load<SoundEffect>("Sound/playerHit");
            bossHit = Content.Load<SoundEffect>("Sound/bossHit");
            tourinionHit = Content.Load<SoundEffect>("Sound/tourinionHit");

            boss.createSoundEffectInstance();
            hero.createSoundEffectInstance();


        }

        protected override void UnloadContent() {

        }

        protected override void Update(GameTime gameTime) {


            if (gameState.Equals("Credits")) {
                if (currentSong != 0) {
                    currentSong = 0;
                    MediaPlayer.Play(mainMenuSong);
                    //MediaPlayer.Volume = 1f;
                }
            } else if (gameState.Equals("Main Menu")) {
                if (currentSong != 0) {
                    currentSong = 0;
                    MediaPlayer.Play(mainMenuSong);
                        //MediaPlayer.Volume = 1f;
                }
            } else if (gameState.Equals("PreGame")) { 
                Initialize();

                gameState = "Game";
                menu.gameState = gameState;
            } else if (gameState.Equals("Game")) {
                if (currentSong != 1) {
                    if (currentSong == 2) {
                        //MediaPlayer.Volume = 1f;
                    } else {
                        MediaPlayer.Play(mainGameSong);
                    }
                    currentSong = 1;
                }


                hero.SetGameTime(gameTime);
                boss.SetGameTime(gameTime);
                tourinion.SetGameTime(gameTime);

                hero.shooter.SetGameTime(gameTime);
                boss.shooter.SetGameTime(gameTime);
                testCollision();

                testHeroShots();
                testBossShots();

                // Threads
                // ThreadPool cria Threads e se encarrega de enfileirá-las e executá-las
                ThreadPool.QueueUserWorkItem(new WaitCallback(Hero.Update), hero);
                ThreadPool.QueueUserWorkItem(new WaitCallback(Boss.Update), boss);
                ThreadPool.QueueUserWorkItem(new WaitCallback(Tourinion.Update), tourinion);
                ThreadPool.QueueUserWorkItem(new WaitCallback(Shooter.UpdateBullets), hero.shooter);
                ThreadPool.QueueUserWorkItem(new WaitCallback(Shooter.UpdateBullets), boss.shooter);

                checkWinConditions();
                checkLoseConditions();

            } else if (gameState.Equals("How To Play")) {
                if (currentSong != 0) {
                    currentSong = 0;
                    MediaPlayer.Play(mainMenuSong);
                    //MediaPlayer.Volume = 1f;
                }
            } else if (gameState.Equals("Pause")) {
                if (currentSong != 2) {
                    if (currentSong == 1) {
                        //MediaPlayer.Volume = 0.3f;
                    } else {
                        MediaPlayer.Play(mainGameSong);
                    }
                    currentSong = 2;
                }
            } else if (gameState.Equals("You Win")) {
                if (currentSong != 2) {
                    if (currentSong == 1) {
                        //MediaPlayer.Volume = 0.3f;
                    } else {
                        MediaPlayer.Play(mainGameSong);
                    }
                    currentSong = 2;
                }
            } else if (gameState.Equals("You Lose")) {
                if (currentSong != 2) {
                    if (currentSong == 1) {
                        //MediaPlayer.Volume = 0.3f;
                    } else {
                        MediaPlayer.Play(mainGameSong);
                    }
                    currentSong = 2;
                }
            } else if (gameState.Equals("Quit")) {
                Exit();
            }

            if(!gameState.Equals(menu.gameState)) {
                gameState = menu.gameState;
                
            }

            menu.Update();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            if (gameState.Equals("Credits")) {
                GraphicsDevice.Clear(Color.Gold);
            } else if (gameState.Equals("Main Menu")) {
                GraphicsDevice.Clear(Color.Navy);
            } else if (gameState.Equals("Game")) {
                GraphicsDevice.Clear(Color.CornflowerBlue);
                spriteBatch.Begin();
                spriteBatch.Draw(floor, floorRect, Color.White);
                spriteBatch.End();

                hero.animation.Draw(spriteBatch, hero.motion.position);
                hero.lifebar.Draw(spriteBatch);

                boss.animation.Draw(spriteBatch, boss.motion.position);
                boss.lifebar.Draw(spriteBatch);

                tourinion.animation.Draw(spriteBatch, tourinion.motion.position);

                hero.shooter.DrawBullets(spriteBatch);
                boss.shooter.DrawBullets(spriteBatch);
            } else if (gameState.Equals("How To Play")) {
                GraphicsDevice.Clear(Color.ForestGreen);
            } else if (gameState.Equals("Pause")) {
                GraphicsDevice.Clear(Color.Black);
            } else if (gameState.Equals("You Win")) {
                GraphicsDevice.Clear(Color.Gold);
            } else if (gameState.Equals("You Lose")) {
                GraphicsDevice.Clear(Color.DarkRed);
            } else if (gameState.Equals("Quit")) {

            }

            menu.Draw();
            base.Draw(gameTime);

        }

        void loadBulletSprites() {
            int totalHeroBullets = hero.shooter.bullets.Count - 1;
            for (int i = 0; i < totalHeroBullets; i++) {
                hero.shooter.bullets[i].animation.spriteSheet = Content.Load<Texture2D>("bullet1");
            }

            int totalBossBullets = boss.shooter.bullets.Count - 1;
            for (int i = 0; i < totalBossBullets; i++) {
                boss.shooter.bullets[i].animation.spriteSheet = Content.Load<Texture2D>("bullet2");
            }
        }

        void testCollision() {
            if (hero.collider.boundaries.Intersects(boss.collider.boundaries)) {
                hero.life.takeDamage(boss.life.damage);
                hero.lifebar.Update(hero.life.life);
                hero.life.setTemporaryInvencibility();
                heroHit.Play();
            }

            if (hero.collider.boundaries.Intersects(tourinion.collider.boundaries)) {
                hero.life.takeDamage(tourinion.life.damage);
                hero.lifebar.Update(hero.life.life);
                hero.life.setTemporaryInvencibility();
                heroHit.Play();
            }
        }

        public void testHeroShots() {
            int totalHeroBullets = hero.shooter.bullets.Count - 1;

            for (int i = 0; i < totalHeroBullets; i++) {
                if (hero.shooter.checkBulletCollision(hero.shooter.bullets[i], boss.collider)) {
                    hero.shooter.sendBulletToStagingPosition(hero.shooter.bullets[i]);
                    boss.life.takeDamage(hero.life.damage);
                    boss.lifebar.Update(boss.life.life);
                    bossHit.Play();
                }

                if (hero.shooter.checkBulletCollision(hero.shooter.bullets[i], tourinion.collider)) {
                    hero.shooter.sendBulletToStagingPosition(hero.shooter.bullets[i]);
                    tourinionHit.Play();
                }
            }
        }

        public void testBossShots() {
            int totalBossBullets = boss.shooter.bullets.Count - 1;

            for (int i = 0; i < totalBossBullets; i++) {
                if (boss.shooter.checkBulletCollision(boss.shooter.bullets[i], hero.collider)) {
                    boss.shooter.sendBulletToStagingPosition(boss.shooter.bullets[i]);
                    hero.life.takeDamage(boss.life.damage);
                    hero.life.setTemporaryInvencibility();
                    hero.lifebar.Update(hero.life.life);
                    heroHit.Play();
                }

                if (boss.shooter.checkBulletCollision(boss.shooter.bullets[i], tourinion.collider)) {
                    boss.shooter.sendBulletToStagingPosition(boss.shooter.bullets[i]);
                    tourinionHit.Play();
                }
            }
        }

        void checkWinConditions() {
            if (boss.life.life <= 0) {
                gameState = "You Win";
                menu.gameState = gameState;
            }
        }

        void checkLoseConditions() {
            if (hero.life.life <= 0) {
                gameState = "You Lose";
                menu.gameState = gameState;
            }
        }
    }
}
