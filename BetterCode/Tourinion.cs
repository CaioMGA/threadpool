﻿using Microsoft.Xna.Framework;
using System;

namespace BetterCode {
    class Tourinion {
        // Tourinion sempre persegue o jogador, que é representado por um objeto da classe Hero
        public Animation animation;
        public Motion motion;
        public CollisionBox collider;
        public Life life;
        Hero player;
        GameTime gt;

        public Tourinion(int animationFrameTime, int frameWidth, int frameHeight, int framesPerAnimation, float _speed, int posX, int posY, int collisionGroup, bool isTrigger, int _life, int _damage, Hero _player) {
            animation = new Animation(animationFrameTime, new Vector2(frameWidth, frameHeight), framesPerAnimation);
            motion = new Motion(_speed, new Vector2(posX, posY));
            Rectangle collisionBoxBoundaries = new Rectangle(posX, posY, frameWidth, frameHeight);
            collider = new CollisionBox(collisionBoxBoundaries, collisionGroup, isTrigger);
            life = new Life(_life, _damage);
            player = _player;
            animation.useFacingDirection = false;

        }

        public static void Update(object stateInfo) {
            Tourinion localTourinion = (Tourinion)stateInfo;
            Vector2 movementDirection = localTourinion.getDirectionTowardsPlayer();
            localTourinion.motion.Update(movementDirection);
            localTourinion.animation.Update(localTourinion.gt, localTourinion.animation.facingDirection, false);
            localTourinion.collider.boundaries.X = (int)localTourinion.motion.position.X;
            localTourinion.collider.boundaries.Y = (int)localTourinion.motion.position.Y;
            localTourinion.life.Update(localTourinion.gt);
            Console.WriteLine(localTourinion.player.motion.position);
        }


        public void SetGameTime(GameTime gameTime) {
            gt = gameTime;
        }

        Vector2 getDirectionTowardsPlayer() {
            // cria um vetor de movimento na direção do jogador
            Vector2 dir = new Vector2(0, 0);

            if (motion.position.X < player.motion.position.X) {
                dir.X = 1;
            } else if (motion.position.X > player.motion.position.X) {
                dir.X = -1;
            }
            if (motion.position.Y < player.motion.position.Y) {
                dir.Y = 1;
            } else if (motion.position.Y > player.motion.position.Y) {
                dir.Y = -1;
            }
            return dir;
        }
    }   
}