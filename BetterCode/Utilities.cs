﻿using Microsoft.Xna.Framework;

namespace BetterCode {
    class Utilities {
        public Vector2 facingDirectionToMovementDirection(int facingDirection) {

            Vector2 movementDirection = new Vector2(0, 0);

            switch (facingDirection) {
                case 0: //UP
                movementDirection.Y = -1;
                break;

                case 1: //UPRIGHT
                movementDirection.X = 1;
                movementDirection.Y = -1;
                break;

                case 2://RIGHT
                movementDirection.X = 1;
                break;

                case 3://DOWNRIGHT
                movementDirection.X = 1;
                movementDirection.Y = 1;
                break;

                case 4://DOWN
                movementDirection.Y = 1;
                break;

                case 5://DOWNLEFT
                movementDirection.X = -1;
                movementDirection.Y = 1;
                break;

                case 6://LEFT
                movementDirection.X = -1;
                break;

                case 7://UPLEFT
                movementDirection.X = -1;
                movementDirection.Y = -1;
                break;
            }

            return movementDirection;
        }

        public int movementDirectionToFacingDirection(Vector2 movementDirection) {

            int facingDirection = 0;
            if (movementDirection.Y > 0) { //down
                if (movementDirection.X > 0) { //right
                    facingDirection = 3;
                } else if (movementDirection.X < 0) { //left
                    facingDirection = 5;
                } else {
                    facingDirection = 4;
                }
            } else if (movementDirection.Y < 0) { //up
                if (movementDirection.X > 0) { //right
                    facingDirection = 1;
                } else if (movementDirection.X < 0) { //left
                    facingDirection = 6;
                } else {
                    facingDirection = 0;
                }
            } else {
                if (movementDirection.X > 0) { //right
                    facingDirection = 2;
                } else if (movementDirection.X < 0) { //left
                    facingDirection = 6;
                }
            }
            
            return facingDirection;
        }
    }
}
