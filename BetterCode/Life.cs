﻿using Microsoft.Xna.Framework;

namespace BetterCode {
    class Life {
        public int life;
        public int damage;
        public bool invencible;
        public int invencibleTime;
        public int elapsedInvencibleTime;

        public Life(int _life, int _damage) {
            life = _life;
            damage = _damage;
            invencible = false;
            invencibleTime = 200;
        }

        public Life(int _life, int _damage, int _invencibleTime) {
            life = _life;
            damage = _damage;
            invencible = false;
            invencibleTime = _invencibleTime;
        }

        public void takeDamage(int dmg) {
            if (!invencible) {
                life -= dmg;
            }
        }

        public void Update(GameTime gameTime) {
            if (invencible) {
                if (elapsedInvencibleTime <= invencibleTime) {
                    elapsedInvencibleTime += gameTime.ElapsedGameTime.Milliseconds;
                } else {
                    invencible = false;
                }

            }
        }

        public void setTemporaryInvencibility() {
            if (!invencible) {
                invencible = true;
                elapsedInvencibleTime = 0;
            }
        }

        public void setInvencible(bool value) {
            invencible = value;
        }
    }
}
