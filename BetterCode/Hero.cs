﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace BetterCode {
    class Hero {
        public Input input = new Input();
        public Animation animation;
        public Motion motion;
        public CollisionBox collider;
        public Life life;
        public Lifebar lifebar;

        public Shooter shooter;
        public Vector2 center;
        public Vector2 centerSprite;

        //GameTime gt = new GameTime();
        GameTime gt;

        Utilities utils = new Utilities();

        //sons
        public SoundEffect shot;
        SoundEffectInstance shotInstance;

        public void createSoundEffectInstance() {
            shotInstance = shot.CreateInstance();
        }

        public Hero(int animationFrameTime, int frameWidth, int frameHeight, int framesPerAnimation, float speed, int posX, int posY, int collisionGroup, bool isTrigger, int timeBetweenShots, int _life, int _damage) {
            input = new Input();
            animation = new Animation(animationFrameTime, new Vector2(frameWidth, frameHeight), framesPerAnimation);
            motion = new Motion(speed, new Vector2(posX, posY));
            Rectangle collisionBoxBoundaries = new Rectangle(posX, posY, frameWidth, frameHeight);
            collider = new CollisionBox(collisionBoxBoundaries, collisionGroup, isTrigger);
            shooter = new Shooter(timeBetweenShots, 10f, 20);
            life = new Life(_life, _damage);
            centerSprite = new Vector2(frameWidth / 2, frameHeight / 2);
        }

        public static void Update(object stateInfo) {
            //chamar SetGameTime() antes de chamar esta funcao
            Hero localHero = (Hero)stateInfo;
            localHero.input.Update();
            localHero.motion.Update(localHero.input.hAxis, localHero.input.vAxis, localHero.input.isAimingButtonPress);
            localHero.animation.Update(localHero.gt, localHero.motion.facingDirection, localHero.input.idle);
            localHero.collider.Update(localHero.motion.position);
            localHero.life.Update(localHero.gt);

            //atira
            if (localHero.input.isShootButtonPress) {
                localHero.UpdateCenter();
                Vector2 shootDirection = new Vector2(localHero.input.hShootingAxis, localHero.input.vShootingAxis);
                if (localHero.shooter.canShoot) {
                    localHero.shotInstance.Play();
                }
                localHero.shooter.shoot(
                    shootDirection,
                    localHero.center);
            }
        }

        void UpdateCenter() {
            center = centerSprite + motion.position;
        }

        public void SetGameTime(GameTime gameTime) {
            gt = gameTime;
        }
    }
}